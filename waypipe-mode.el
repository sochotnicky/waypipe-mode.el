;;; waypipe-mode.el --- Make clipboard work for emacsclient over waypipe  -*- lexical-binding: t -*-
;; Author: Stanislav Ochotnický <stanislav@ochotnicky.com>
;; Version: 0.0.6
;; File: waypipe-mode.el
;; Package-Requires: ((emacs "27.1"))
;; Keywords: tools
;; URL: https://gitlab.freedesktop.org/sochotnicky/waypipe-mode.el

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Module for switching to using wl-clipboard for pasting/cutting text and
;; setting WAYLAND_DISPLAY environmen variable for programs
;;
;; Mostly useful when Emacs is run under Wayland and especially over waypipe or
;; similar.  Once wl-clipboard is used it will use WAYLAND_DISPLAY of current
;; frame to copy and paste text so that the copying/pasting is not happing on
;; remote waypipe end
;;

;;; Dependencies:
;; wl-clipboard package installed on the system

;;; Code:
;; Save original values

;;;; Variables
(defvar waypipe-orig-interprogram-cut interprogram-cut-function
  "Original interprogram cut/copy function.")

(defvar waypipe-orig-interprogram-paste interprogram-paste-function
  "Original interprogram paste function.")

(defvar waypipe-orig-wayland-display (getenv "WAYLAND_DISPLAY")
  "Original value of WAYLAND_DISPLAY environmen variable.")

(defvar waypipe-wl-copy-process nil
  "Process object for wl-copy.")

;;;; waypipe-mode

;;;###autoload
(define-minor-mode waypipe-mode
  "A minor mode to use wl-clipboard over waypipe.

This can be useful when running emacsclient over waypipe.  The
server started with WAYLAND_DISPLAY set for local instance and so
copy/paste would not work correctly over waypipe.  We fix
copy/paste by overriding `interprogram-cut-function' and
`interprogram-paste-function' with custom methods relying on
wl-clipboard (wl-copy and wl-paste) when we detect current frame
to be running over waypipe.

In addition we set WAYLAND_DISPLAY so any open applications that
use WAYLAND_DISPLAY will open on the host runnning waypipe, not
where the server is running."
  :group 'waypipe
  :global t

  (if waypipe-mode
      (add-hook 'post-command-hook #'waypipe--maybe-enable)
      (remove-hook 'post-command-hook #'waypipe--maybe-enable)))

(defun waypipe--disable-override()
  "Disable waypipe override for cut/paste functions and WAYLAND_DISPLAY."
  (setq interprogram-cut-function waypipe-orig-interprogram-cut
        interprogram-paste-function waypipe-orig-interprogram-paste)
  (setenv "WAYLAND_DISPLAY" waypipe-orig-wayland-display))

(defun waypipe--enable-override()
  "Enable waypipe override for cut/paste functions and WAYLAND_DISPLAY."
  (setq waypipe-orig-interprogram-cut interprogram-cut-function
        waypipe-orig-interprogram-paste interprogram-paste-function
        waypipe-orig-wayland-display (getenv "WAYLAND_DISPLAY")
        interprogram-cut-function 'waypipe--wl-copy
        interprogram-paste-function 'waypipe--wl-paste)
  (setenv "WAYLAND_DISPLAY" (frame-parameter (selected-frame) 'display)))

(defun waypipe--maybe-enable()
  "Enable waypipe mode if not already enabled and we are in waypipe frame.

  Otherwise disable waypipe mode."
  (if (and (not (waypipe--is-enabled-p)) (waypipe--is-waypipe-frame-p))
      (waypipe--enable-override))
  (if (and (waypipe--is-enabled-p) (not (waypipe--is-waypipe-frame-p)))
      (waypipe--disable-override)))

(defun waypipe--is-waypipe-frame-p()
  "Return non-nil when current frame is displayed over waypipe connection."
  (when-let ((wid (frame-parameter (selected-frame) 'display)))
    (string-match "wayland-[a-zA-Z0-9]\\{8\\}" wid)))


(defun waypipe--is-enabled-p()
  "Return non-nil when waypipe mode is active."
  (not (and (eq interprogram-cut-function waypipe-orig-interprogram-cut)
            (eq interprogram-paste-function waypipe-orig-interprogram-paste))))

;; Originally from
;; https://gist.github.com/yorickvP/6132f237fbc289a45c808d8d75e0e1fb
;; Added WAYLAND_DISPLAY override so that this works over waypipe
(defun waypipe--wl-copy (text)
  "Copy TEXT into a new wl-copy process buffer."
  (setq waypipe-wl-copy-process
          (make-process :name "wl-copy"
                        :buffer nil
                        :command '("wl-copy" "-f" "-n")
                        :connection-type 'pipe))
  (process-send-string waypipe-wl-copy-process text)
  (process-send-eof waypipe-wl-copy-process))

(defun waypipe--wl-paste ()
  "Paste text from Wayland clipboard using wl-paste."
  (if (and waypipe-wl-copy-process (process-live-p waypipe-wl-copy-process))
      nil ; should return nil if we're the current paste owner
      (shell-command-to-string "wl-paste -n | tr -d \r")))

(provide 'waypipe-mode)

;;; waypipe-mode.el ends here
