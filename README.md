# waypipe-mode.el - working copy/paste through waypipe

Emacs minor mode for handling copy-paste/executing thing running emacsclient
over waypipe.

## Usage 

You can [download latest release](https://gitlab.freedesktop.org/sochotnicky/waypipe-mode.el/-/releases)
from Gitlab release page.

Then, M-x package-install-file RET waypipe-mode-x.y.z.el RET

Or you can use straight:
```
(use-package waypipe-mode
  :straight (waypipe-mode 
              :type git :host nil 
              :repo "https://gitlab.freedesktop.org/sochotnicky/waypipe-mode.el")
  :config
  (waypipe-mode 1))
```

Once `waypipe-mode` is enabled it will try to keep track whether your current
frame is running over waypipe and adjust copy/paste methods accordingly.

## Authors and acknowledgment

Original snippet that showed me how to do this was [here](https://gist.github.com/yorickvP/6132f237fbc289a45c808d8d75e0e1fb).

## License
GPLv3 - see COPYING for details
