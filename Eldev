; -*- mode: emacs-lisp; lexical-binding: t -*-

;; Uncomment some calls below as needed for your project.
                                        ;(eldev-use-package-archive 'gnu-elpa)
                                        ;(eldev-use-package-archive 'nongnu-elpa)
                                        ;(eldev-use-package-archive 'melpa)

(eldev-use-plugin 'autoloads)


(eldev-defcommand
 org_yaa-md5 (&rest _)
 "Create md5 checksum of .tar and .el files in dist folder."
 (mapc
  (lambda (file)
    (write-region
     (secure-hash 'md5 file)
     nil
     (concat (file-name-sans-extension file) ".md5")))
  (append
   (directory-files eldev-dist-dir t "\\.tar\\'")
   (directory-files eldev-dist-dir t "\\.el\\'"))))

(eldev-defcommand
 org_yaa-release (&rest _)
 "Release the next version.

Choose the next version, prompt for release notes, update all
files in the `main' fileset, update the NEWS file, commit all
changes, and create a new annotated tag."
 (defun version-to-string (version) (mapconcat #'number-to-string version "."))
 (let* ((files (eldev-find-files (eldev-standard-fileset 'main)))
        (current (package-desc-version (eldev-package-descriptor)))
        (major (version-to-string (list (+ 1 (car current)) 0 0)))
        (minor (version-to-string (list (car current) (+ 1 (cadr current)) 0)))
        (patch (version-to-string (list (car current) (cadr current) (+ 1 (caddr current)))))
        (choice (read-string (format "Current version: %s\n\n(M) Major\t%s\n(m) Minor\t%s\n(p) Patch\t%s\n\nM/m/p: "
                                     (version-to-string current) major minor patch)))
        (next (cond ((string= "M" choice) major)
                    ((string= "m" choice) minor)
                    ((string= "p" choice) patch)
                    (t (error "Invalid input")))))
   (when (= 0 (call-process "git" nil nil nil "commit"
                            "--allow-empty"
                            "-m" (format "Release %s\n\n\n\n#\n# Write a message for release:\n#\t%s"
                                         next next)
                            "-e"))
     (let ((release-notes (shell-command-to-string "git show -s --format=%b")))
       (dolist (file files)
         (if (string= "el" (file-name-extension file))
             (save-window-excursion
               (find-file file)
               (when (or (lm-header "package-version") (lm-header "version"))
                 (message "Updating %s" file)
                 (delete-char (length (version-to-string current)))
                 (insert next)
                 (save-buffer 0)
                 (call-process "git" nil nil nil "add" file)))))
       (with-temp-file "NEWS"
         (message "Updating NEWS")
         (insert-file-contents "NEWS")
         (forward-line)
         (insert (format "\n* %s\n\n%s" next release-notes)))
       (call-process "git" nil nil nil "add" "NEWS")
       (call-process "git" nil nil nil "commit" "--amend" "--no-edit")
       (message "Creating tag %s" next)
       (call-process "git" nil nil nil "tag" next "-a" "-m" release-notes)))))
